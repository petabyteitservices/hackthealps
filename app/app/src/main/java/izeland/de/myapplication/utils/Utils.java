package izeland.de.myapplication.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import izeland.de.myapplication.R;

import static izeland.de.myapplication.utils.Utils.ActivityType.Aufstiegsanlagen;
import static izeland.de.myapplication.utils.Utils.ActivityType.Berg;
import static izeland.de.myapplication.utils.Utils.ActivityType.LaufenUndFitness;
import static izeland.de.myapplication.utils.Utils.ActivityType.Loipen;
import static izeland.de.myapplication.utils.Utils.ActivityType.Pferdesport;
import static izeland.de.myapplication.utils.Utils.ActivityType.Piste;
import static izeland.de.myapplication.utils.Utils.ActivityType.Radfahren;
import static izeland.de.myapplication.utils.Utils.ActivityType.Rodelbahnen;
import static izeland.de.myapplication.utils.Utils.ActivityType.Stadtrundgang;
import static izeland.de.myapplication.utils.Utils.ActivityType.Wandern;

public class Utils
{

	public static final Map<ActivityType, Integer> ACTIVITY_TYPE_ICON_MAP = new HashMap<>();

	static
	{
		ACTIVITY_TYPE_ICON_MAP.put(Berg, R.drawable.mountain);
		ACTIVITY_TYPE_ICON_MAP.put(Radfahren, R.drawable.bike);
		ACTIVITY_TYPE_ICON_MAP.put(Stadtrundgang, R.drawable.city);
		ACTIVITY_TYPE_ICON_MAP.put(Pferdesport, R.drawable.horseriding);
		ACTIVITY_TYPE_ICON_MAP.put(Wandern, R.drawable.hike);
		ACTIVITY_TYPE_ICON_MAP.put(LaufenUndFitness, R.drawable.running);
		ACTIVITY_TYPE_ICON_MAP.put(Loipen, R.drawable.ski);
		ACTIVITY_TYPE_ICON_MAP.put(Rodelbahnen, R.drawable.sledging);
		ACTIVITY_TYPE_ICON_MAP.put(Piste, R.drawable.ski);
		ACTIVITY_TYPE_ICON_MAP.put(Aufstiegsanlagen, R.drawable.ski);
	}

	public enum ActivityType
	{
		Berg("Berg", 1),
		Radfahren("Radfahren", 2),
		Stadtrundgang("Stadtrundgang", 4),
		Pferdesport("Pferdesport", 8),
		Wandern("Wandern", 16),
		LaufenUndFitness("Laufen und Fitness", 32),
		Loipen("Loipen", 64),
		Rodelbahnen("Rodelbahnen", 128),
		Piste("Piste", 256),
		Aufstiegsanlagen("Aufstiegsanlagen", 512);

		private String name;
		private int value;

		ActivityType(String name, int value)
		{
			this.name = name;
			this.value = value;
		}

		public String getName()
		{
			return name;
		}

		public int getValue()
		{
			return value;
		}

		public static ActivityType getActivityType(String name)
		{
			for (ActivityType activityType : ActivityType.values())
			{
				if (activityType.name.equals(name))
				{
					return activityType;
				}
			}
			return null;
		}
	}

	public static List<ActivityType> constructSearch(Set<Integer> selectedDrawables)
	{
		List<ActivityType> selectedActivityTypes = new ArrayList<>();
		for (int selectedDrawable : selectedDrawables)
		{
			for (Map.Entry<ActivityType, Integer> activityTypeIntegerEntry : ACTIVITY_TYPE_ICON_MAP.entrySet())
			{
				if (activityTypeIntegerEntry.getValue() == selectedDrawable)
				{
					selectedActivityTypes.add(activityTypeIntegerEntry.getKey());
				}
			}
		}

		return selectedActivityTypes;
	}


	public static final int[] JOURNEY_ICONS = {
//			R.drawable.baby,
			R.drawable.bike,
//			R.drawable.car,
//			R.drawable.caravan,
			R.drawable.city,
//			R.drawable.family,
//			R.drawable.fire,
//			R.drawable.handicaped,
			R.drawable.hike,
			R.drawable.horseriding,
//			R.drawable.hunting,
//			R.drawable.man,
			R.drawable.mountain,
//			R.drawable.mtb,
			R.drawable.running,
//			R.drawable.sight,
//			R.drawable.ski,
			R.drawable.sledging,
//			R.drawable.train,
//			R.drawable.woman
	};

	public static int weekDayIconSwap(int weekday)
	{
		switch (weekday)
		{
			case R.drawable.mo_off:
				return R.drawable.mo_on;
			case R.drawable.tu_off:
				return R.drawable.tu_on;
			case R.drawable.we_off:
				return R.drawable.we_on;
			case R.drawable.th_off:
				return R.drawable.th_on;
			case R.drawable.fr_off:
				return R.drawable.fr_on;
			case R.drawable.sa_off:
				return R.drawable.sa_on;
			case R.drawable.su_off:
				return R.drawable.su_on;

			case R.drawable.mo_on:
				return R.drawable.mo_off;
			case R.drawable.tu_on:
				return R.drawable.tu_off;
			case R.drawable.we_on:
				return R.drawable.we_off;
			case R.drawable.th_on:
				return R.drawable.th_off;
			case R.drawable.fr_on:
				return R.drawable.fr_off;
			case R.drawable.sa_on:
				return R.drawable.sa_off;
			case R.drawable.su_on:
				return R.drawable.su_off;
			default:
				return -1;
		}
	}


	public static int getWeekDayIcon(int weekDay)
	{
		switch (weekDay)
		{
			case 2:
				return R.drawable.mo_off;
			case 3:
				return R.drawable.tu_off;
			case 4:
				return R.drawable.we_off;
			case 5:
				return R.drawable.th_off;
			case 6:
				return R.drawable.fr_off;
			case 7:
				return R.drawable.sa_off;
			case 1:
				return R.drawable.su_off;
			default:
				return -1;
		}
	}

	public static int getNavigationId(int index)
	{
		switch (index)
		{
			case 0:
				return R.id.navigation_profile;
			case 1:
				return R.id.navigation_dashboard;
			case 2:
				return R.id.navigation_connect;
			default:
				return -1;
		}
	}
}
