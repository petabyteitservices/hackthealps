package izeland.de.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreen extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen_activity);
		findViewById(android.R.id.content).setBackground(getDrawable(R.drawable.splash_background));

		Timer timer = new Timer();
		timer.schedule(new TimerTask()
		{
			@Override
			public void run()
			{
				startActivity(new Intent(SplashScreen.this, MainActivity.class));
				finish();
			}
		}, 1500);
	}
}
