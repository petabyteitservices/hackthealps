package de.hellwig.hackthealps_app.hotspotdata.parsing;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import de.hellwig.hackthealps_app.hotspotdata.HotSpot;
import izeland.de.myapplication.R;

public class WuerthParser
{

	private static final String TAG = "WUERTH";

	public ArrayList<AlyvixDeviceState> parse(final Context context)
	{
//		new Thread(new Runnable()
//		{
//			@Override
//			public void run()
//			{
		InputStream file = context.getResources().openRawResource(R.raw.hacktheapls_data);
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file));

		ArrayList<AlyvixDeviceState> arrayList = new ArrayList<>();

		try
		{
			String line;
			bufferedReader.readLine();
			while ((line = bufferedReader.readLine()) != null)
			{
				AlyvixDeviceState state = readLine(line);
				if (state != null)
				{
					arrayList.add(state);
				}
			}
		}
		catch (Exception e)
		{
			Log.e(TAG, "run: ", e);
		}


		Collections.sort(arrayList);

//				Set<String> locations = new HashSet<>();
//
//				for (AlyvixDeviceState alyvixDeviceState : arrayList)
//				{
//					if (alyvixDeviceState != null)
//					{
//						locations.add("\n" + alyvixDeviceState.getLocation().getLatitude() + " " + alyvixDeviceState.getLocation().getLongitude() + " " + alyvixDeviceState.getRadius());
//					}
//				}
//
//				for (String location : locations)
//				{
//					System.out.println(location);
//				}
//			}
//		}).start();

		return arrayList;
	}


	@Nullable
	private AlyvixDeviceState readLine(String line)
	{
		AlyvixDeviceState alyvixDeviceState;
		final String[] elements = line.split(",");

		if (elements.length != 6)
		{
			return null;
		}

		double radius;
		int duration;
		HotSpot.State state;
		try
		{
			state = HotSpot.State.getState(elements[4]);
			radius = Double.parseDouble(elements[2]);
			duration = (state == HotSpot.State.NOT_EXECUTED) ? 100000 : Integer.parseInt(elements[3]) ;
		}
		catch (NumberFormatException n)
		{
			Log.w(TAG, "Can't read radius or duration for line: " + line, n);
			return null;
		}

		SimpleDateFormat inputFormat;
		Date date;
		try
		{
			inputFormat = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss.SSS'+00:00'", Locale.ENGLISH);
			date = inputFormat.parse(elements[0]);
		}
		catch (ParseException pe1)
		{
			Log.i(TAG, "Can't read date for line: " + line + ", now trying again with a different DateFormat...");
			try
			{
				inputFormat = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss'+00:00'", Locale.ENGLISH);
				date = inputFormat.parse(elements[0]);
			}
			catch (ParseException pe2)
			{
				Log.w(TAG, "Still Can't read date for line: " + line + ", now trying again with a different "
						+ "DateFormat...", pe2);
				return null;
			}
		}

		alyvixDeviceState = new AlyvixDeviceState(date, elements[1], radius, duration, state, GeoHash.fromString(elements[5]).getCenter());
		return alyvixDeviceState;
	}

}
