package de.hellwig.hackthealps_app.mapview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLngBounds;

import java.lang.ref.WeakReference;
import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.List;

import de.hellwig.hackthealps_app.hotspotdata.HotSpotProvider;
import de.hellwig.hackthealps_app.hotspotdata.HotSpot;
import de.hellwig.hackthealps_app.mapview.circlerendering.CircleGradientRenderer;

public class ParseAndDrawHotSpotsOnMapAsyncTask extends AsyncTask<Void, Void, ParseAndDrawHotSpotsOnMapAsyncTask.AsyncTaskResult>
{

	private static final int MEASUREMENTS_TO_TAKE_INTO_ACCOUNT = 1000;
	//    private static final float MIN_DURATION = 29708.f;
	private static final float MIN_DURATION = 29708.f;
	//    private static final float MAX_DURATION = 50625.f;
	//    private static final float MAX_DURATION = 201670.f;
	private static final float MAX_DURATION = 100000.f;
	private static final float CIRCLE_TRANSPARENCY = 0.3f;

	static class AsyncTaskResult
	{
		List<GroundOverlayOptions> groundOverlayOptionsList;
		LatLngBounds mapBounds;
		float minDuration;
		float maxDuration;
	}

	private static final String TAG = "ParseAndDrawHotSpotsOnMapAsyncTask";

	private WeakReference<Context> context;
	private WeakReference<GoogleMap> map;

	public ParseAndDrawHotSpotsOnMapAsyncTask(Context context, GoogleMap map)
	{
		this.context = new WeakReference<>(context);
		this.map = new WeakReference<>(map);
	}

	@Override
	protected AsyncTaskResult doInBackground(Void... params)
	{
		HotSpotProvider hotSpotProvider = new HotSpotProvider(context.get().getApplicationContext());
		List<HotSpot> hotSpots = hotSpotProvider.getHotSpots();

		LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
		List<GroundOverlayOptions> groundOverlayOptionsList = new LinkedList<>();
		float minDuration = Float.MAX_VALUE;
		float maxDuration = Float.MIN_VALUE;

		for (HotSpot hotSpot : hotSpots)
		{
			List<HotSpot.Measurement> measurements = new LinkedList<>();
			for (int i = 0; i < MEASUREMENTS_TO_TAKE_INTO_ACCOUNT; i++)
			{
				//noinspection EmptyCatchBlock
				try
				{
					HotSpot.Measurement measurement = hotSpot.getMeasurements().pop();
					measurements.add(measurement);

				}
				catch (EmptyStackException ese)
				{
				}
			}

			double[] durations = new double[measurements.size()];
			int maxRadius = 0;
			// ToDo: State gar nicht mehr mit reinnehmen? Falls doch, wie verwursten?
			HotSpot.Measurement measurement;
			for (int i = 0; i < measurements.size(); i++)
			{
				measurement = measurements.get(i);
				durations[i] = measurement.duration;

				if (measurement.radius > maxRadius)
				{
					maxRadius = measurement.radius;
				}

				if (measurement.duration > maxDuration)
				{
					maxDuration = measurement.duration;
				}

				if (measurement.duration < minDuration)
				{
					minDuration = measurement.duration;
				}
			}

			GroundOverlayOptions overlayOptions = new GroundOverlayOptions()
					.image(BitmapDescriptorFactory
							.fromBitmap(CircleGradientRenderer
									.renderCircleGradientBitmap(MIN_DURATION, MAX_DURATION, durations)))
					.position(hotSpot.getLocation(), maxRadius)
					.transparency(CIRCLE_TRANSPARENCY);
			groundOverlayOptionsList.add(overlayOptions);

			boundsBuilder.include(hotSpot.getLocation());
		}

		AsyncTaskResult result = new AsyncTaskResult();
		result.groundOverlayOptionsList = groundOverlayOptionsList;
		result.mapBounds = boundsBuilder.build();
		result.minDuration = minDuration;
		result.maxDuration = maxDuration;
		return result;
	}

	@SuppressLint("LongLogTag")
	@Override
	protected void onPostExecute(AsyncTaskResult result)
	{
		if (context.get() == null)
		{
			Log.w(TAG, "Could not proceed with the AsyncTask result, Context is null.");
			return;
		}
		if (map.get() == null)
		{
			Log.w(TAG, "Could not proceed with the AsyncTask result, GoogleMap is null.");
			return;
		}

		for (GroundOverlayOptions overlayOptions : result.groundOverlayOptionsList)
		{
			map.get().addGroundOverlay(overlayOptions);
		}

		// ToDo: Fix when changing from fullscreen map view!
		int width = context.get().getResources().getDisplayMetrics().widthPixels;
		int height = context.get().getResources().getDisplayMetrics().heightPixels;
		map.get().animateCamera(CameraUpdateFactory.newLatLngBounds(result.mapBounds, width, height, 100));

		String log = "min duration = " + result.minDuration + "\nmax duration = " + result.maxDuration;
		Log.d(TAG, log);
		//        Toast.makeText(this, log, Toast.LENGTH_LONG).show();
	}
}
