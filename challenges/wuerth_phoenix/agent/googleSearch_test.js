
Feature('Search on Google');

Scenario('for dolomites', (I) => {
  I.amOnPage('https://www.google.com');
  within('#tsf', () => {
    I.fillField('.gsfi', 'dolomites');
    I.click('Google Search');
  });
  I.see('Ergebnisse');
});
