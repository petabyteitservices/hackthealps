package izeland.de.myapplication.model;

import java.util.HashSet;
import java.util.Set;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PhoneStore
{

	private static final String KEY_JOURNEY_FILTER_IDS = "KEY_JOURNEY_FILTER_IDS";

	@SuppressLint("StaticFieldLeak") // It's an application context
	private static PhoneStore phoneStore;
	private SharedPreferences defaultSharedPreferences;


	private PhoneStore(Application context)
	{
		defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
	}

	public static PhoneStore getInstance(Application context)
	{
		if (phoneStore == null)
		{
			phoneStore = new PhoneStore(context);
		}
		return phoneStore;
	}

	public void setJourneyFilterIds(Set<Integer> ids)
	{
		Set<String> set = new HashSet<>();

		for (Integer id : ids)
		{
			set.add(Integer.toString(id));
		}

		defaultSharedPreferences.edit().putStringSet(KEY_JOURNEY_FILTER_IDS, set).apply();
	}

	public Set<Integer> getJournesFilterIds()
	{
		Set<Integer> set = new HashSet<>();
		Set<String> stringSet = defaultSharedPreferences.getStringSet(KEY_JOURNEY_FILTER_IDS, new HashSet<String>());
		for (String s : stringSet)
		{
			set.add(Integer.parseInt(s));
		}

		return set;
	}
}
