package com.bosch.myspin.virtualapps.music.ui.cache;

import java.lang.ref.WeakReference;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.bosch.myspin.connectedcommon.util.ExternalScreenHelper;

/**
 * An {@link BitmapDrawable} which may load its content asynchronously.
 *
 * https://developer.android.com/training/displaying-bitmaps/process-bitmap.html
 *
 * @author Malte Pein
 * @since 19.10.16
 */
public class AsyncBitmapDrawable extends BitmapDrawable
{

    private final WeakReference<BitmapWorkerTask> mBitmapWorkerTaskReference;

    /**
     * Creates a new {@link AsyncBitmapDrawable} with a {@link WeakReference} on the given {@link BitmapWorkerTask}.
     *
     * @param res
     *         the {@link Resources}.
     * @param bitmap
     *         a {@link Bitmap} as placeholder until the bitmapWorkerTask is ready and loaded the read image.
     * @param bitmapWorkerTask
     *         the {@link BitmapWorkerTask}.
     */
    AsyncBitmapDrawable(Resources res, Bitmap bitmap, BitmapWorkerTask bitmapWorkerTask)
    {
        super(res, bitmap);
        mBitmapWorkerTaskReference = new WeakReference<>(bitmapWorkerTask);
    }

    /**
     * Loads a {@link Bitmap} into a new {@link AsyncBitmapDrawable}.
     *
     * @param resources
     *         the {@link Resources}.
     * @param placeHolderBitmap
     *         a placeholder {@link Bitmap}, until the given Bitmap is loaded.
     * @param filename
     *         a {@link Uri} to the Bitmap to load.
     * @param imageView
     *         the {@link ImageView} to set the {@link Bitmap} to.
     */
    public static void loadBitmap(final Resources resources, final Bitmap placeHolderBitmap, final Uri filename,
                                  final ImageView imageView)
    {
        if (cancelPotentialWork(filename, imageView))
        {
            final Bitmap bitmap = BitmapCache.getInstance().getBitmapFromMemCache(filename);
            if (bitmap != null)
            {
                imageView.setImageBitmap(bitmap);
            }
            else
            {
                final BitmapWorkerTask bitmapWorkerTask = new BitmapWorkerTask(imageView, true);
                final AsyncBitmapDrawable asyncBitmapDrawable =
                        new AsyncBitmapDrawable(resources, placeHolderBitmap, bitmapWorkerTask);
                imageView.setImageDrawable(asyncBitmapDrawable);
                bitmapWorkerTask.execute(filename);
            }
        }
    }

    /**
     * Loads a {@link Bitmap} into a new {@link AsyncBitmapDrawable}.
     *
     * @param resources
     *         the {@link Resources}.
     * @param placeHolderBitmap
     *         a placeholder {@link Bitmap}, until the given Bitmap is loaded.
     * @param filename
     *         a {@link Uri} to the Bitmap to load.
     * @param imageView
     *         the {@link ImageView} to set the {@link Bitmap} to.
     */
    public static void loadBitmapWithoutCache(final Resources resources, final Bitmap placeHolderBitmap,
                                              final Uri filename, final ImageView imageView)
    {
        if (cancelPotentialWork(filename, imageView))
        {
            final BitmapWorkerTask bitmapWorkerTask = new BitmapWorkerTask(imageView, false);
            final AsyncBitmapDrawable asyncBitmapDrawable =
                    new AsyncBitmapDrawable(resources, placeHolderBitmap, bitmapWorkerTask);
            imageView.setImageDrawable(asyncBitmapDrawable);
            bitmapWorkerTask.execute(filename);
        }
    }

    /**
     * Cancels the potentially running {@link BitmapWorkerTask}.
     *
     * @param filename
     *         the associated filename
     * @param imageView
     *         the {@link ImageView} to set the Bitmap to.
     *
     * @return true if a new task with the given parameters may be started, false if there is already a task running or
     * if the task could not be canceled.
     */
    private static boolean cancelPotentialWork(Uri filename, ImageView imageView)
    {
        final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

        if (bitmapWorkerTask != null)
        {
            final Uri bitmapData = bitmapWorkerTask.mResourceUrl;
            // If bitmapData is not yet set or it differs from the new resourceUrl
            if (bitmapData == null || bitmapData != filename)
            {
                // Cancel previous task
                bitmapWorkerTask.cancel(true);
            }
            else
            {
                // The same work is already in progress
                return false;
            }
        }
        // No task associated with the ImageView, or an existing task was cancelled
        return true;
    }

    /**
     * Returns the {@link BitmapWorkerTask} associated with the given {@link ImageView}.
     *
     * @param imageView
     *         the {@link ImageView} to find the associated {@link BitmapWorkerTask}.
     *
     * @return the associated {@link BitmapWorkerTask}.
     */
    private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView)
    {
        if (imageView != null)
        {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncBitmapDrawable)
            {
                final AsyncBitmapDrawable asyncBitmapDrawable = (AsyncBitmapDrawable) drawable;
                return asyncBitmapDrawable.mBitmapWorkerTaskReference.get();
            }
        }
        return null;
    }

    /**
     * A class to load an image - given with an {@link Uri} - asynchronously into a {@link Bitmap}.
     */
    static class BitmapWorkerTask extends AsyncTask<Uri, Void, Bitmap>
    {

        private final WeakReference<ImageView> mImageViewReference;
        private static int mSize;
        private Uri mResourceUrl;
        private boolean mCache;

        /**
         * Creates a new {@link BitmapWorkerTask}.
         *
         * @param imageView
         *         the {@link ImageView} to put the loaded {@link Bitmap}.
         * @param cache
         *         flag if the loaded image should be added to the cache.
         */
        private BitmapWorkerTask(ImageView imageView, boolean cache)
        {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            mImageViewReference = new WeakReference<>(imageView);
            if (mSize == 0)
            {
                mSize = (int) ExternalScreenHelper.getRowHeight();
            }

            mCache = cache;
        }

        @Override
        protected Bitmap doInBackground(final Uri... params)
        {
            // Decode image in background.
            mResourceUrl = params[0];
            if (mImageViewReference.get() != null)
            {
                Bitmap bitmap = decodeSampledBitmapFromResource(mResourceUrl, mSize, mSize);
                if (mCache)
                {
                    BitmapCache.getInstance().addBitmapToMemoryCache(mResourceUrl, bitmap);
                }
                return bitmap;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap)
        {
            if (isCancelled())
            {
                bitmap = null;
            }

            if (mImageViewReference.get() != null && bitmap != null)
            {
                final ImageView imageView = mImageViewReference.get();
                final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);
                if (this == bitmapWorkerTask)
                {
                    imageView.setImageBitmap(bitmap);
                }
            }
        }

        /**
         * Calculates the size the bitmap should have.
         *
         * @param options
         *         the BitmapFactory.Options
         * @param reqWidth
         *         the required width
         * @param reqHeight
         *         the required  height.
         *
         * @return the needed size.
         */
        private static int calculateInSampleSize(
                BitmapFactory.Options options, int reqWidth, int reqHeight)
        {
            // Raw height and width of image
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth)
            {
                final int halfHeight = height / 2;
                final int halfWidth = width / 2;

                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
                // height and width larger than the requested height and width.
                while ((halfHeight / inSampleSize) >= reqHeight
                        && (halfWidth / inSampleSize) >= reqWidth)
                {
                    inSampleSize *= 2;
                }
            }

            return inSampleSize;
        }

        /**
         * Decodes the {@link Bitmap}.
         *
         * @param fileName
         *         Uri to the {@link Bitmap} source.
         * @param reqWidth
         *         the required width.
         * @param reqHeight
         *         the required  height.
         *
         * @return the decoded {@link Bitmap}.
         */
        private static Bitmap decodeSampledBitmapFromResource(Uri fileName, int reqWidth, int reqHeight)
        {
            if (fileName != null)
            {
                // First decode with inJustDecodeBounds=true to check dimensions
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(fileName.toString(), options);

                // Calculate inSampleSize
                options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

                // Decode bitmap with inSampleSize set
                options.inJustDecodeBounds = false;
                return BitmapFactory.decodeFile(fileName.toString(), options);
            }
            return null;
        }
    }
}
