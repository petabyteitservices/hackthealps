package de.hellwig.hackthealps_app.mapview.circlerendering;

import android.graphics.Color;
import android.support.annotation.ColorInt;

import de.hellwig.hackthealps_app.hotspotdata.HotSpot;

import static de.hellwig.hackthealps_app.hotspotdata.HotSpot.State.CRITICAL;
import static de.hellwig.hackthealps_app.hotspotdata.HotSpot.State.NOT_EXECUTED;
import static de.hellwig.hackthealps_app.hotspotdata.HotSpot.State.OK;
import static de.hellwig.hackthealps_app.hotspotdata.HotSpot.State.TIMED_OUT;
import static de.hellwig.hackthealps_app.hotspotdata.HotSpot.State.UNDEFINED;
import static de.hellwig.hackthealps_app.hotspotdata.HotSpot.State.WARNING;

@SuppressWarnings({"unused", "WeakerAccess"})
final class StateRatingColorHelper
{
    private static final @ColorInt int GREY = Color.argb(255, 200, 200, 200);
    private static final @ColorInt int GREEN = Color.argb(255, 0, 255, 0);
    private static final @ColorInt int RED = Color.argb(255, 255, 0, 0);
    private static final @ColorInt int ORANGE = Color.argb(255, 255, 128, 0);
    private static final @ColorInt int YELLOW = Color.argb(255, 255, 255, 0);
    private static final @ColorInt int PINK = Color.argb(255, 255, 0, 255);

    @ColorInt
    static int getColorForHotSpotState(final HotSpot.State state)
    {
        switch (state)
        {
            case OK: return GREEN;
            case WARNING: return YELLOW;
            case CRITICAL: return ORANGE;
            case TIMED_OUT: return RED;
            case NOT_EXECUTED: return GREY;
            case UNDEFINED:
            default: return PINK;
        }
    }

    // rating is in interval [0.0; 1.0] where 0.0 is the "not working at all case"
    static HotSpot.State getHotSpotStateForRating(double rating)
    {
        if (rating > 1. || rating < 0. ) return UNDEFINED;
        if (rating <= 1. && rating > 0.75 ) return OK;
        if (rating <= 0.75 && rating > 0.5 ) return WARNING;
        if (rating <= 0.5 && rating > 0.25 ) return CRITICAL;
        if (rating <= 0.25 && rating > 0.0 ) return TIMED_OUT;
        /*if (rating == 0. )*/ return NOT_EXECUTED;
    }

    // mainly used for making statistics
    static double getRatingForHotSpotState(final HotSpot.State state)
    {
        switch (state)
        {
            case OK: return 0.875;
            case WARNING: return 0.625;
            case CRITICAL: return 0.375;
            case TIMED_OUT: return 0.125;
            case NOT_EXECUTED: return 0.0;
            case UNDEFINED:
            default: return -1;
        }
    }

    // rating is in interval [0.0; 1.0] where 0.0 is the "not working at all case"
    @ColorInt
    static int getColorForHotSpotRating(double rating)
    {
        return getColorForHotSpotState(getHotSpotStateForRating(rating));
    }

    // rating is in interval [0.0; 1.0] where 0.0 is the "not working at all case"
    @ColorInt
    static int getColorForHotSpotMeasurementDuration(double minDuration, double maxDuration, double duration)
    {
        float fraction = (float)((duration - minDuration) / (maxDuration - minDuration));
        fraction = Math.max(0.f, Math.min(1.f, fraction));

        int resultColor;

//        Log.d("DURATION", "min = " + minDuration + ", max = " + maxDuration + ", duration = " + duration);
//        Log.d("COLOR", "fraction = " + fraction);// + ", color = " + Color.valueOf(resultColor).toString());

        resultColor = interpolateColor(GREEN, RED, fraction);

        return resultColor;
    }

    private static float interpolate(float a, float b, float proportion) {
        return (a + ((b - a) * proportion));
    }

    /**
     * Returns an interpolated color, between <code>a</code> and <code>b</code>
     * proportion = 0, results in color a
     * proportion = 1, results in color b
     */
    private static int interpolateColor(int a, int b, float proportion) {

        if (proportion > 1 || proportion < 0) {
            throw new IllegalArgumentException("proportion must be [0 - 1]");
        }
        float[] hsva = new float[3];
        float[] hsvb = new float[3];
        float[] hsv_output = new float[3];

        Color.colorToHSV(a, hsva);
        Color.colorToHSV(b, hsvb);
        for (int i = 0; i < 3; i++) {
            hsv_output[i] = interpolate(hsva[i], hsvb[i], proportion);
        }

        int alpha_a = Color.alpha(a);
        int alpha_b = Color.alpha(b);
        float alpha_output = interpolate(alpha_a, alpha_b, proportion);

        return Color.HSVToColor((int) alpha_output, hsv_output);
    }
}
