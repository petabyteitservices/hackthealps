package izeland.de.myapplication.bitmap;


import android.graphics.Bitmap;
import android.net.Uri;
import android.util.LruCache;

/**
 * {@link Bitmap} Cache caches Bitmaps in a {@link LruCache}. This class is a singleton and should be released after
 * usage.
 *
 * @author Malte Pein
 */
public class BitmapCache
{
	private static BitmapCache mInstance = new BitmapCache();
	private LruCache<Uri, Bitmap> mMemoryCache;


	/**
	 * Returns the single instance of this class.
	 *
	 * @return the single instance of this class.
	 */
	public static BitmapCache getInstance()
	{
		if (mInstance == null)
		{
			mInstance = new BitmapCache();
		}
		return mInstance;
	}

	/**
	 * Creates a new BitmapCache.
	 */
	private BitmapCache()
	{
		// Get max available VM memory, exceeding this amount will throw an
		// OutOfMemory exception. Stored in kilobytes as LruCache takes an
		// int in its constructor.
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		final int cacheSize = maxMemory / 10; // 10 % of memory:

		mMemoryCache = new LruCache<Uri, Bitmap>(cacheSize)
		{
			@Override
			protected int sizeOf(Uri key, Bitmap bitmap)
			{
				// The cache size will be measured in kilobytes rather than
				// number of items.
				return bitmap.getByteCount() / 1024;
			}
		};
	}

	/**
	 * Empties the cache and removes the instance reference.
	 */
	public synchronized void release()
	{
		mMemoryCache.evictAll();
		mInstance = null;
	}

	/**
	 * Add a new Bitmap to the memory cache.
	 *
	 * @param key
	 * 		the key to store the bitmap with
	 * @param bitmap
	 * 		the loaded {@link Bitmap}.
	 */
	public synchronized void addBitmapToMemoryCache(Uri key, Bitmap bitmap)
	{
		if (key != null && bitmap != null && getBitmapFromMemCache(key) == null)
		{
			mMemoryCache.put(key, bitmap);
		}
	}

	/**
	 * Returns null or the Bitmap associated with the given key.
	 *
	 * @param key
	 * 		the key.
	 *
	 * @return the bitmap if cached.
	 */
	public synchronized Bitmap getBitmapFromMemCache(Uri key)
	{
		if (key == null)
		{
			return null;
		}
		return mMemoryCache.get(key);
	}
}
