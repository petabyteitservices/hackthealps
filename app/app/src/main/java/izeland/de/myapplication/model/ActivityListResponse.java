package izeland.de.myapplication.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ActivityListResponse
{
    @SerializedName("Items")
    public List<SouthTyrolActivity> items;
}
