package de.hellwig.hackthealps_app.mapview.circlerendering;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public final class CircleGradientRenderer
{
    private static final int BITMAP_RADIUS = 500; // in pixel

    public static Bitmap renderCircleGradientBitmap(double minRating, double maxRating, double... ratings)
    {
        // ratings are in interval [0.0; 1.0] where 0.0 is the "not working at all case"
        // the order of the ratings within the array represents the chronological sequence of the ratings (from now
        // to the past)

        final int size = BITMAP_RADIUS * 2;

        Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(Color.TRANSPARENT);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);

        //noinspection UnnecessaryLocalVariable
        final float startRadius = BITMAP_RADIUS - 2;
        final float radiusDecrease = (startRadius - 3) / (float)ratings.length;
        float currentRadius = startRadius - 3;

        // draw black border
        paint.setStrokeWidth(4);
        paint.setColor(Color.BLACK);
        canvas.drawCircle(BITMAP_RADIUS, BITMAP_RADIUS, currentRadius, paint);

        // draw concentric rings
        for (double rating : ratings)
        {
            paint.setStrokeWidth(2);
            paint.setColor(StateRatingColorHelper.getColorForHotSpotMeasurementDuration(minRating, maxRating, rating));
            canvas.drawCircle(BITMAP_RADIUS, BITMAP_RADIUS, currentRadius, paint);
            currentRadius -= radiusDecrease;
        }

        return bitmap;
    }
}
