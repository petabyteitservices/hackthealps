package izeland.de.myapplication.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import izeland.de.myapplication.R;
import izeland.de.myapplication.bitmap.AsyncBitmapDrawable;

public class ShowOnUrlImageFragment extends Fragment
{

	private ImageView imageView;
	private Object image;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{

		imageView = new ImageView(getContext());

		Uri uri = Uri.parse("https://doc.lts.it/docsite/ImageRender.aspx?ID=139975e5ef9106ea3d5a939418ec5424");
		Bitmap bitmapTemp = BitmapFactory.decodeResource(getResources(), R.drawable.gais);
		AsyncBitmapDrawable.loadBitmapWithoutCache(getResources(), bitmapTemp, uri, imageView);

		return imageView;
	}

}
