package izeland.de.myapplication.model;

import java.util.List;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class SouthTyrolActivity
{

	@SerializedName("Id")
	public String id;

	@SerializedName("Type")
	public String type;

	@SerializedName("Detail")
	public Map<String, Descrption> details;

	public class Descrption
	{
		@SerializedName("Title")
		public String title;

		@SerializedName("BaseText")
		public String baseText;
	}

	@SerializedName("ImageGallery")
	public List<Map<String, Object>> image;

	@SerializedName("GpsInfo")
	public List<GpsInfo> gpsInfoList;

	public class GpsInfo
	{
		@SerializedName("Gpstype")
		public String gpsType;

		@SerializedName("Latitude")
		public double latitude;

		@SerializedName("Longitude")
		public double longitude;
	}

}
