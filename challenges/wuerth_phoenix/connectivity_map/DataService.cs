using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CsvHelper;

namespace connectivity_map
{
    public class DataService
    {
        private const string Base32 = "0123456789bcdefghjkmnpqrstuvwxyz";
        private static readonly int[] Bits = new[] { 16, 8, 4, 2, 1 };

        private Dictionary<DataItem, List<DataSet>> geoHashes = new Dictionary<DataItem, List<DataSet>>();

        public DataService()
        {
            ReadCsvData();
        }

        public Dictionary<DataItem, List<DataSet>> DataSet
        {
            get
            {
                return geoHashes;
            }
        }

        private void ReadCsvData()
        {
            using (var reader = File.OpenText("/Users/petersacchet/Development/HACK/hackthealps/challenges/wuerth_phoenix/connectivity_map/Data/hacktheapls_data.csv"))
            {
                var csv = new CsvReader(reader);
                // By type
                var records = csv.GetRecords<CsvDataItem>().ToList();

                foreach (var item in records)
                {
                    DataItem dataItem = CreateItem(item);
                    if (!geoHashes.ContainsKey(dataItem))
                    {
                        geoHashes[dataItem] = new List<DataSet>();
                    }
                    var dataSet = CreateDataSet(item);

                    geoHashes[dataItem].Add(dataSet);
                }
                Debug.Write(geoHashes.Count);
            }
        }

        private DataSet CreateDataSet(CsvDataItem item)
        {
            return new DataSet
            {
                timestamp = item.timestamp,
                radius = item.radius,
                duration = item.duration,
                state = item.state

            };
        }

        private DataItem CreateItem(CsvDataItem item)
        {
            var latlong = this.Decode(item.geohash);
            return new DataItem
            {
                Name = item.name,
                lat = latlong[0],
                lng = latlong[1]
            };
        }

        public double[] Decode(String geohash)
        {
            bool even = true;
            double[] lat = { -90.0, 90.0 };
            double[] lon = { -180.0, 180.0 };

            foreach (char c in geohash)
            {
                int cd = Base32.IndexOf(c);
                for (int j = 0; j < 5; j++)
                {
                    int mask = Bits[j];
                    if (even)
                    {
                        RefineInterval(ref lon, cd, mask);
                    }
                    else
                    {
                        RefineInterval(ref lat, cd, mask);
                    }
                    even = !even;
                }
            }

            return new[] { (lat[0] + lat[1]) / 2, (lon[0] + lon[1]) / 2 };
        }
        public static void RefineInterval(ref double[] interval, int cd, int mask)
        {
            if ((cd & mask) != 0)
            {
                interval[0] = (interval[0] + interval[1]) / 2;
            }
            else
            {
                interval[1] = (interval[0] + interval[1]) / 2;
            }
        }
    }
}