
Feature('Facebook');

Scenario('logs in a user', (I) => {
  I.amOnPage('https://www.facebook.com/');
  within('#login_form', () => {
    I.fillField('#email', 'm0rd@gmx.de');
    I.fillField('#pass', '5YS0JFb4Ze6A');
    I.click('Log In');
  });
  I.waitForElement('.uiHeaderTitle');
  I.see('Pete Alps');
});

Scenario('posts a picture', (I) => {
  I.amOnPage('https://www.facebook.com/');
  within('#login_form', () => {
    I.fillField('#email', 'm0rd@gmx.de');
    I.fillField('#pass', '5YS0JFb4Ze6A');
    I.click('Log In');
  });
  I.waitForElement('#universalNav');
  I.click('News Feed');
  I.waitForElement('#pagelet_composer');
  I.see("What's on your mind, Pete?");

  //TODO: actually post a picture
});
