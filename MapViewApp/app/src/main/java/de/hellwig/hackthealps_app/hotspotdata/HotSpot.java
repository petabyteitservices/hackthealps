package de.hellwig.hackthealps_app.hotspotdata;

import java.util.Stack;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

@SuppressWarnings("unused")
public final class HotSpot
{
    private String name;
    private LatLng location;
    private Stack<Measurement> measurements = new Stack<>();

    HotSpot(String name, double latitude, double longitude)
    {
        this.name = name;
        this.location = new LatLng(latitude, longitude);
    }

    HotSpot(String name, Location location)
    {
        this.name = name;
        this.location = new LatLng(location.getLatitude(), location.getLongitude());
    }

    HotSpot(String name, LatLng location)
    {
        this.name = name;
        this.location = location;
    }

    String getName()
    {
        return name;
    }

    public LatLng getLocation()
    {
        return location;
    }

    public Stack<Measurement> getMeasurements()
    {
        return measurements;
    }

    void addMeasurement(long timestamp, int radius, int duration, State state)
    {
        Measurement measurement = new Measurement();
        measurement.timestamp = timestamp;
        measurement.radius = radius;
        measurement.duration = duration;
        measurement.state = state;

        measurements.add(measurement);
    }

    public enum State
    {
        OK("ok"),
        WARNING("warning"),
        CRITICAL("critical"),
        TIMED_OUT("timed_out"),
        NOT_EXECUTED("not_executed"),
        UNDEFINED("undefined");

        private String name;

        State(String name)
        {
            this.name = name;
        }

        public static State getState(String name)
        {
            for (State state : State.values())
            {
                if (state.name.equals(name))
                {
                    return state;
                }
            }
            return null;
        }
    }

    public class Measurement
    {
        long timestamp; // unix time
        public int radius; // in meters
        public int duration; // in ms
        public State state;
    }
}
