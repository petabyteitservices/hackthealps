package de.hellwig.hackthealps_app.hotspotdata.parsing;

import java.util.Date;

import android.location.Location;
import android.support.annotation.NonNull;

import de.hellwig.hackthealps_app.hotspotdata.HotSpot.State;

public class AlyvixDeviceState implements Comparable<AlyvixDeviceState>
{

	private Date date;
	private String name;
	private double radius;
	private int duration;
	private State state;
	private Location location;


	public AlyvixDeviceState(Date date, String name, double radius, int duration, State state, Location location)
	{
		this.date = date;
		this.name = name;
		this.radius = radius;
		this.duration = duration;
		this.state = state;
		this.location = location;
	}

	public Date getDate()
	{
		return date;
	}

	public String getName()
	{
		return name;
	}

	public double getRadius()
	{
		return radius;
	}

	public int getDuration()
	{
		return duration;
	}

	public State getState()
	{
		return state;
	}

	public Location getLocation()
	{
		return location;
	}

	@Override
	public int compareTo(@NonNull AlyvixDeviceState o)
	{
		return (int) Math.signum(o.duration - duration);

//		return (int) Math.signum(o.date.getTime() - date.getTime());
	}
}
