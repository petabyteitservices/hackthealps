package izeland.de.myapplication.fragments.profile;


import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Set;

import izeland.de.myapplication.MainActivity;
import izeland.de.myapplication.R;
import izeland.de.myapplication.fragments.ChildFragment;
import izeland.de.myapplication.model.PhoneStore;
import izeland.de.myapplication.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements View.OnClickListener, ChildFragment
{

	private View fragmentContainer;
	private ViewGroup journeyGroupContainer;
	private ViewGroup daysFilterContainer;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
		rootView.findViewById(R.id.current_filters).setOnClickListener(this);
		fragmentContainer = rootView.findViewById(R.id.fragment_container);
		journeyGroupContainer = rootView.findViewById(R.id.current_filters);
		daysFilterContainer = rootView.findViewById(R.id.days_filter);

		fillJourneyField();
		//fillVehicleFilter();
		fillDaysFilter();

		return rootView;
	}

	private void fillJourneyField()
	{
		journeyGroupContainer.removeAllViews();
		Set<Integer> ids = PhoneStore.getInstance(getActivity().getApplication()).getJournesFilterIds();
		for (Integer id : ids)
		{
			final ImageView imageView = new ImageView(getContext());
			imageView.setImageResource(id);
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			layoutParams.height = 150;
			layoutParams.weight = 150;
			layoutParams.bottomMargin = 10;
			layoutParams.topMargin = 10;
			layoutParams.leftMargin = 10;
			layoutParams.rightMargin = 10;
			journeyGroupContainer.addView(imageView, layoutParams);
		}

		if (journeyGroupContainer.getChildCount() == 0)
		{
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			layoutParams.bottomMargin = 25;
			layoutParams.topMargin = 25;
			layoutParams.leftMargin = 25;
			layoutParams.rightMargin = 25;
			TextView textView = new TextView(getContext());
			textView.setTextSize(22);
			textView.setText("Click here to describe you journey!");
			journeyGroupContainer.addView(textView, layoutParams);
		}
	}


	private void fillDaysFilter()
	{
		int startIndex = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
		for (int i = startIndex - 1; i < 8 + startIndex; i++)
		{
			final ImageView imageView = new ImageView(getContext());
			imageView.setImageResource(Utils.getWeekDayIcon(i % 7 + 1));
			imageView.setOnClickListener(this);
			imageView.setTag(Utils.getWeekDayIcon(i % 7 + 1));

			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			layoutParams.height = 170;
			layoutParams.weight = 170;
			layoutParams.bottomMargin = 10;
			layoutParams.topMargin = 10;
			layoutParams.leftMargin = 8;
			layoutParams.rightMargin = 8;
			daysFilterContainer.addView(imageView, layoutParams);
		}
	}

	@Override
	public void onClick(View v)
	{
		if (v instanceof ImageView)
		{
			// Handle of week days:
			ImageView imageView = (ImageView) v;
			imageView.setImageResource(Utils.weekDayIconSwap((Integer) imageView.getTag()));
			imageView.setTag(Utils.weekDayIconSwap((Integer) imageView.getTag()));
			if (imageView.isSelected())
			{
				imageView.setSelected(false);
//				imageView.setColorFilter(ContextCompat.getColor(getContext(), android.R.color.black), PorterDuff.Mode.SRC_IN);
			}
			else
			{
				imageView.setSelected(true);
//				imageView.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.DST_IN);
			}
		}
		else
		{
			DragAndDropFrag dragAndDropFrag = new DragAndDropFrag();
			dragAndDropFrag.setChildFragmentDelegate(this);
			getChildFragmentManager().beginTransaction().replace(R.id.fragment_container, dragAndDropFrag).commit();
			fragmentContainer.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onChildBackPress()
	{
		Fragment frag = getChildFragmentManager().findFragmentById(R.id.fragment_container);
		if (frag != null)
		{
			getChildFragmentManager().beginTransaction().remove(frag).commit();
			fragmentContainer.setVisibility(View.GONE);
			fillJourneyField();
			((MainActivity)getActivity()).triggerUpdate();
		}
	}
}
