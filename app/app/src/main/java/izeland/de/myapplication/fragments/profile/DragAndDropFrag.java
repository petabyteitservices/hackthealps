package izeland.de.myapplication.fragments.profile;

import android.content.ClipData;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.HashSet;
import java.util.Set;

import izeland.de.myapplication.R;
import izeland.de.myapplication.fragments.ChildFragment;
import izeland.de.myapplication.model.PhoneStore;
import izeland.de.myapplication.utils.Utils;

public class DragAndDropFrag extends Fragment implements View.OnDragListener, View.OnLongClickListener
{

	private ViewGroup dragBox;
	private ViewGroup dropBox;
	private ChildFragment childFragmentDelegate;


	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{

		final View rootView = inflater.inflate(R.layout.drag_drop_frag, container, false);

		dragBox = rootView.findViewById(R.id.dragBox);
		dropBox = rootView.findViewById(R.id.dropBox);
		rootView.findViewById(R.id.ok_button).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (childFragmentDelegate != null)
				{
					saveState();
					childFragmentDelegate.onChildBackPress();
				}
			}
		});

		Set<Integer> journeyFilter = PhoneStore.getInstance(getActivity().getApplication()).getJournesFilterIds();

		for (int icon : Utils.JOURNEY_ICONS)
		{
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			layoutParams.height = 150;
			layoutParams.weight = 150;
			layoutParams.bottomMargin = 10;
			layoutParams.topMargin = 10;
			layoutParams.leftMargin = 10;
			layoutParams.rightMargin = 10;

			if (journeyFilter.contains(icon))
			{
				dropBox.addView(createImageView(icon), layoutParams);
			}
			else
			{
				dragBox.addView(createImageView(icon), layoutParams);
			}
		}

		dropBox.setOnDragListener(this);
		dragBox.setOnDragListener(this);

		return rootView;
	}

	private View createImageView(int icon)
	{
		final ImageView imageView = new ImageView(getContext());
		imageView.setImageResource(icon);
		imageView.setOnLongClickListener(this);
		imageView.setId(View.generateViewId());
		imageView.setTag(icon);
		return imageView;
	}

	@Override
	public boolean onDrag(View v, DragEvent event)
	{
		final View dragView = (View) event.getLocalState();
		final ViewGroup container = (ViewGroup) v;

		switch (event.getAction())
		{
			case DragEvent.ACTION_DRAG_STARTED:
				if (container.indexOfChild(dragView) != -1)
				{
					container.removeView(dragView);
				}
				break;
			case DragEvent.ACTION_DRAG_ENTERED:
				break;
			case DragEvent.ACTION_DRAG_EXITED:
				break;
			case DragEvent.ACTION_DROP:
				View view = (View) event.getLocalState();
				if (view.getParent() == null)
				{
					container.addView(view);
					view.setVisibility(View.VISIBLE);
				}
				else
				{
					return false;
				}
				break;
			case DragEvent.ACTION_DRAG_ENDED:
				if (dragView.getParent() == null)
				{
					if (container.equals(dragBox))
					{
						dragBox.addView(dragView);
					}
					else
					{
						dropBox.addView(dragView);
					}
				}
				break;
			default:
				break;
		}

		return true;
	}

	private void saveState()
	{
		Set<Integer> set = new HashSet<>();
		for (int i = 0; i < dropBox.getChildCount(); i++)
		{
			set.add((Integer) dropBox.getChildAt(i).getTag());
		}

		PhoneStore.getInstance(getActivity().getApplication()).setJourneyFilterIds(set);
	}

	@Override
	public boolean onLongClick(View v)
	{
		ClipData clipData = ClipData.newPlainText(" ", " ");
		View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
		v.startDragAndDrop(clipData, shadowBuilder, v, 0);
		return true;
	}

	public void setChildFragmentDelegate(ChildFragment childFragmentDelegate)
	{
		this.childFragmentDelegate = childFragmentDelegate;
	}
}
