package de.hellwig.hackthealps_app.hotspotdata;

import java.util.List;

public interface HotSpotProvider
{
    List<HotSpot> getHotSpots();

//    HotSpot getCentroid();
}
