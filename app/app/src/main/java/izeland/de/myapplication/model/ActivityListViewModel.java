package izeland.de.myapplication.model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import java.util.List;

import izeland.de.myapplication.utils.Utils;

public class ActivityListViewModel extends AndroidViewModel
{

	private MutableLiveData<ActivityListResponse> activityListLiveData;

	public ActivityListViewModel(@NonNull Application application)
	{
		super(application);

		List<Utils.ActivityType> activityTypes = Utils.constructSearch(PhoneStore.getInstance(getApplication()).getJournesFilterIds());
		int sum = 0;
		for (Utils.ActivityType activityType : activityTypes)
		{
			sum += activityType.getValue();
		}

		if (sum != 0)
		{
			activityListLiveData = ActivityListRepo.getInstance().getActivityListForTypeLiveData(sum);
		}
		else
		{
			activityListLiveData = ActivityListRepo.getInstance().getActivityListForTypeLiveData(null);
		}
	}

	public LiveData<ActivityListResponse> getActivityListObservable()
	{
		return activityListLiveData;
	}

	public void update(Utils.ActivityType activityType)
	{
		ActivityListRepo.getInstance().updateActivityListForTypeLiveData(activityType, activityListLiveData);
	}

	public void update(int value)
	{
		ActivityListRepo.getInstance().updateActivityListForTypeLiveData(value, activityListLiveData);
	}

	private static final MutableLiveData MUTABLE_LIVE_DATA = new MutableLiveData();

	static
	{
		MUTABLE_LIVE_DATA.setValue(null);
	}
}
