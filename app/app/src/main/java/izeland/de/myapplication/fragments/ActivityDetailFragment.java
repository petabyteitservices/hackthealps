package izeland.de.myapplication.fragments;

import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import izeland.de.myapplication.R;
import izeland.de.myapplication.bitmap.AsyncBitmapDrawable;
import izeland.de.myapplication.model.SouthTyrolActivity;

public class ActivityDetailFragment extends Fragment implements View.OnClickListener
{

	private ChildFragment childFragmentDelegate;
	private SouthTyrolActivity southTyrolActivity;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
	{
		final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.activity_detail_fragment, container, false);

		final ImageView imageView = rootView.findViewById(R.id.action_image);

		final TextView titleView = rootView.findViewById(R.id.title);

		final WebView webViewDescription = rootView.findViewById(R.id.description_text);

		final TextView gps = rootView.findViewById(R.id.gps_info);

		rootView.findViewById(R.id.ok_button).setOnClickListener(this);

		initFrag(imageView, titleView, webViewDescription, gps);


		return rootView;
	}

	private void initFrag(ImageView imageView, TextView title, WebView webViewDescription, TextView gps)
	{
		//Load image:

		String imageUrl = null;
		if (southTyrolActivity.image.size() > 0)
		{
			imageUrl = (String) southTyrolActivity.image.get(0).get("ImageUrl");
		}

		if (imageUrl != null)
		{
			Uri uri = Uri.parse(imageUrl);
			Bitmap bitmapTemp = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.hike);
			AsyncBitmapDrawable.loadBitmap(getContext().getResources(), bitmapTemp, uri, imageView);
		}

		// set Title:
		title.setText(southTyrolActivity.details.get("en").title);

		// description
		webViewDescription.setBackgroundColor(Color.TRANSPARENT);
		String text = southTyrolActivity.details.get("en").baseText;
		webViewDescription.loadDataWithBaseURL("", text, null, null, null);

		List<SouthTyrolActivity.GpsInfo> locations = southTyrolActivity.gpsInfoList;
		if (locations != null && locations.size() > 0)
		{
			gps.setText("lat: " + locations.get(0).latitude + " lon: " + locations.get(0).longitude);
		}
	}


	public void setChildFragmentDelegate(ChildFragment childFragmentDelegate)
	{
		this.childFragmentDelegate = childFragmentDelegate;
	}

	public void setSouthTyrolActivity(SouthTyrolActivity southTyrolActivity)
	{
		this.southTyrolActivity = southTyrolActivity;
	}

	@Override
	public void onClick(View v)
	{
		childFragmentDelegate.onChildBackPress();
	}
}
