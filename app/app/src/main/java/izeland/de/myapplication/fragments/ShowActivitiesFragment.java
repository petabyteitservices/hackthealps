package izeland.de.myapplication.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import izeland.de.myapplication.R;
import izeland.de.myapplication.bitmap.AsyncBitmapDrawable;
import izeland.de.myapplication.model.ActivityListResponse;
import izeland.de.myapplication.model.ActivityListViewModel;
import izeland.de.myapplication.model.PhoneStore;
import izeland.de.myapplication.model.SouthTyrolActivity;
import izeland.de.myapplication.utils.Utils;

public class ShowActivitiesFragment extends Fragment
		implements Observer<ActivityListResponse>, AdapterView.OnItemClickListener, ChildFragment, SharedPreferences.OnSharedPreferenceChangeListener
{

	private ActivityListViewModel viewModel;
	private ListView listView;
	private ArrayAdapter<SouthTyrolActivity> listAdapter;
	private View fragmentContainer;


	@Override
	public void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		viewModel = ViewModelProviders.of(this).get(ActivityListViewModel.class);

		viewModel.getActivityListObservable().observe(this, this);

		PreferenceManager.getDefaultSharedPreferences(getContext()).registerOnSharedPreferenceChangeListener(this);
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
	                         @Nullable Bundle savedInstanceState)
	{
		final View rootView = inflater.inflate(R.layout.activities_fragment, container, false);
		listView = rootView.findViewById(R.id.list);
		listAdapter = new ShowActivitiesFragment.Adapter(Objects.requireNonNull(getContext()), R.layout.list_entry,
				new LinkedList<SouthTyrolActivity>());
		listView.setAdapter(listAdapter);
		listView.setOnItemClickListener(this);
		fragmentContainer = rootView.findViewById(R.id.fragment_container);

		return rootView;
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		PreferenceManager.getDefaultSharedPreferences(getContext()).unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onChanged(@Nullable ActivityListResponse activitiesList)
	{
		if (activitiesList != null)
		{
			listAdapter.clear();
			listAdapter.addAll(activitiesList.items);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		ActivityDetailFragment activityDetailFragment = new ActivityDetailFragment();
		activityDetailFragment.setSouthTyrolActivity((SouthTyrolActivity) listView.getAdapter().getItem(position));
		activityDetailFragment.setChildFragmentDelegate(this);
		getChildFragmentManager().beginTransaction()
				.replace(R.id.fragment_container, activityDetailFragment)
				.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,
						android.R.anim.slide_in_left, android.R.anim.slide_out_right)
				.commit();
		fragmentContainer.setVisibility(View.VISIBLE);
	}

	@Override
	public void onChildBackPress()
	{
		Fragment frag = getChildFragmentManager().findFragmentById(R.id.fragment_container);
		if (frag != null)
		{
			getChildFragmentManager().beginTransaction().remove(frag).commit();
			fragmentContainer.setVisibility(View.GONE);
		}
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
	{
		List<Utils.ActivityType> activityTypes = Utils.constructSearch(PhoneStore.getInstance(getActivity().getApplication()).getJournesFilterIds());
		int sum = 0;
		for (Utils.ActivityType activityType : activityTypes)
		{
			sum += activityType.getValue();
		}
		if (!activityTypes.isEmpty())
		{
			viewModel.update(sum);
		}
		else
		{
			viewModel.update(null);
		}
	}

	private static class Adapter extends ArrayAdapter<SouthTyrolActivity>
	{

		Adapter(@NonNull Context context, int resource, List<SouthTyrolActivity> value)
		{
			super(context, resource, value);
			clear();
		}

		@NonNull
		@Override
		public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
		{
			if (convertView == null)
			{
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_entry, parent, false);
				convertView.setBackgroundColor(getContext().getColor(R.color.listBackground));
			}

			ImageView imageView = convertView.findViewById(R.id.act_picture);
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			TextView row1 = convertView.findViewById(R.id.act_title);

			SouthTyrolActivity currentItem = getItem(position);

			String imageUrl;
			Bitmap bitmapTemp = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.hike);
			if (currentItem.image.size() > 0)
			{
				imageUrl = (String) currentItem.image.get(0).get("ImageUrl");
				if (imageUrl != null)
				{
					Uri uri = Uri.parse(imageUrl);
					AsyncBitmapDrawable.loadBitmap(getContext().getResources(), bitmapTemp, uri, imageView);
				}
				else
				{
					imageView.setImageBitmap(bitmapTemp);
				}
			}
			else
			{
				imageView.setImageBitmap(bitmapTemp);
			}

			row1.setText(currentItem.details.get("en").title);
			row1.setTextColor(Color.WHITE);

			return convertView;
		}
	}
}
