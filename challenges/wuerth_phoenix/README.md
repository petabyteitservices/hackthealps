# Würth Phoenix

Most people hate being parted from the Internet even when they are on holiday. Many of them might choose a location where they can get a fast and reliable connection while abroad. An emerging Internet provider has decided to propose an innovative solution to communities all over South Tyrol offering tourists and inhabitants always available free Internet at never seen speed. Würth Phoenix was asked to give an answer to the question whether the service holds what it promises.

Würth Phoenix used Alyvix visual synthetic monitoring approach to address the question: A synthetic real user interacts with a browser just as a human would do. Performance of application transactions is measured and reported giving an overview of service availability and responsiveness. Alyvix devices are placed at 80 strategic locations, where the expected coverage by the Internet provider is well known.

These data concerning Internet availability at the 80 strategic locations open the street to quite a few investigations: Where should I go on holiday if Internet connectivity matters a lot to me? Is the provider right in claiming always available free Internet at never seen speed? Where should the provider strengthen its presence to reach maximum impact in terms of satisfied users? Just to mention a few...

## Details on the Alyvix Agent

Simply imagine a small robot doing just a few of the key steps, a real user is expected to do during an average holiday:

* connect to free WIFI
* log into Facebook
* log into Instagram
* share a picture on both socials
* do a search of “dolomites” on google

The robot is expected to repeat these steps at a certain frequency.

The test will give back

* the total test duration (= the time in milliseconds)
* the test state (in case it failed)
* the reason it failed (went into timeout, could not execute some of the steps for some reason)


# More details about the challenge:
https://www.neteye-blog.com/2018/09/hackthealps-challenge-with-wurth-phoenix
