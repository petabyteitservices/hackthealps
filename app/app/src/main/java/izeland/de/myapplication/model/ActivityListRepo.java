package izeland.de.myapplication.model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import izeland.de.myapplication.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static izeland.de.myapplication.model.OpenDataHubApi.BASE_URL;

public class ActivityListRepo
{
	private OpenDataHubApi openDataHubApi;

	private static class SingletonHelper
	{
		private static final ActivityListRepo INSTANCE = new ActivityListRepo();
	}

	public static ActivityListRepo getInstance()
	{
		return SingletonHelper.INSTANCE;
	}

	private ActivityListRepo()
	{
		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(BASE_URL)
				.addConverterFactory(GsonConverterFactory.create())
				.build();

		openDataHubApi = retrofit.create(OpenDataHubApi.class);
	}

	public LiveData<ActivityListResponse> getActivityListLiveData()
	{
		final MutableLiveData<ActivityListResponse> data = new MutableLiveData<>();

		openDataHubApi.listActivities().enqueue(new Callback<ActivityListResponse>()
		{
			@Override
			public void onResponse(Call<ActivityListResponse> call, Response<ActivityListResponse> response)
			{
				if (response.isSuccessful())
				{
					data.setValue(response.body());
				}
			}

			@Override
			public void onFailure(Call<ActivityListResponse> call, Throwable t)
			{
				data.setValue(null);
			}
		});

		return data;
	}

	public MutableLiveData<ActivityListResponse> getActivityListForTypeLiveData(Utils.ActivityType activityType)
	{
		final MutableLiveData<ActivityListResponse> data = new MutableLiveData<>();

		String searchStr;
		if (activityType != null)
		{
			searchStr = activityType.getName();
		}
		else
		{
			searchStr = null;
		}

		openDataHubApi.listActivitiesForType(searchStr).enqueue(new Callback<ActivityListResponse>()
		{
			@Override
			public void onResponse(Call<ActivityListResponse> call, Response<ActivityListResponse> response)
			{
				if (response.isSuccessful())
				{
					data.setValue(response.body());
				}
			}

			@Override
			public void onFailure(Call<ActivityListResponse> call, Throwable t)
			{
				data.setValue(null);
			}
		});

		return data;
	}

	public MutableLiveData<ActivityListResponse> getActivityListForTypeLiveData(int sum)
	{
		final MutableLiveData<ActivityListResponse> data = new MutableLiveData<>();

		openDataHubApi.listActivitiesForType(Integer.toString(sum)).enqueue(new Callback<ActivityListResponse>()
		{
			@Override
			public void onResponse(Call<ActivityListResponse> call, Response<ActivityListResponse> response)
			{
				if (response.isSuccessful())
				{
					data.setValue(response.body());
				}
			}

			@Override
			public void onFailure(Call<ActivityListResponse> call, Throwable t)
			{
				data.setValue(null);
			}
		});

		return data;
	}

	public LiveData<ActivityListResponse> updateActivityListForTypeLiveData(Utils.ActivityType activityType, final MutableLiveData<ActivityListResponse> data)
	{
		String searchStr;
		if (activityType != null)
		{
			searchStr = activityType.getName();
		}
		else
		{
			searchStr = null;
		}

		openDataHubApi.listActivitiesForType(searchStr).enqueue(new Callback<ActivityListResponse>()
		{
			@Override
			public void onResponse(Call<ActivityListResponse> call, Response<ActivityListResponse> response)
			{
				if (response.isSuccessful())
				{
					data.setValue(response.body());
				}
			}

			@Override
			public void onFailure(Call<ActivityListResponse> call, Throwable t)
			{
				data.setValue(null);
			}
		});

		return data;
	}

	public LiveData<ActivityListResponse> updateActivityListForTypeLiveData(int sum, final MutableLiveData<ActivityListResponse> data)
	{
		openDataHubApi.listActivitiesForType(Integer.toString(sum)).enqueue(new Callback<ActivityListResponse>()
		{
			@Override
			public void onResponse(Call<ActivityListResponse> call, Response<ActivityListResponse> response)
			{
				if (response.isSuccessful())
				{
					data.setValue(response.body());
				}
			}

			@Override
			public void onFailure(Call<ActivityListResponse> call, Throwable t)
			{
				data.setValue(null);
			}
		});

		return data;
	}
}
