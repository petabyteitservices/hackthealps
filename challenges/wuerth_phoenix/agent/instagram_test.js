
Feature('Instagram');

Scenario('logs in user', (I) => {
  I.amOnPage('https://www.instagram.com/');
  I.waitForElement('footer');
  I.scrollTo('footer');
  I.click('Log in');
  I.waitForElement('input[name=username]');
  I.fillField('input[name=username]', 'sacchet@kaffeeschluerfer.com');
  I.fillField('input[name=password]', '5YS0JFb4Ze6A');
  I.wait(1);
  I.click('button');
  I.waitForElement('.coreSpriteDesktopNavProfile',5);
});
