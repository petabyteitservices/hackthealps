package de.hellwig.hackthealps_app.mapview;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import izeland.de.myapplication.model.SouthTyrolActivity;
import izeland.de.myapplication.utils.Utils;

public class DrawActivitiesOnMapAsyncTask
		extends AsyncTask<SouthTyrolActivity, Void, DrawActivitiesOnMapAsyncTask.AsyncTaskResult>
{

	private static final float ICON_TRANSPARENCY = 0.3f;
	private static final float ICON_WIDTH = 2000; // in meters

	private static final String TAG = "DrawActivitiesOnMapAsyncTask";

	private WeakReference<GoogleMap> map;

	private static final List<GroundOverlay> currentGroundOverlay = new ArrayList<>();

	public DrawActivitiesOnMapAsyncTask(GoogleMap map)
	{
		this.map = new WeakReference<>(map);
	}

	@Override
	protected final AsyncTaskResult doInBackground(SouthTyrolActivity... activities)
	{
		List<Pair<GroundOverlayOptions, SouthTyrolActivity>> groundOverlayOptionsList = new LinkedList<>();

		for (SouthTyrolActivity activity : activities)
		{
			if (activity.gpsInfoList == null)
			{
				continue;
			}

			for (SouthTyrolActivity.GpsInfo gpsInfo : activity.gpsInfoList)
			{
				GroundOverlayOptions overlayOptions = new GroundOverlayOptions()
						.image(BitmapDescriptorFactory
								.fromResource(Utils.ACTIVITY_TYPE_ICON_MAP
										.get(Utils.ActivityType.getActivityType(activity.type))))
						.position(new LatLng(gpsInfo.latitude, gpsInfo.longitude), ICON_WIDTH)
						.clickable(true)
						.transparency(ICON_TRANSPARENCY);
				groundOverlayOptionsList.add(new Pair<>(overlayOptions, activity));
			}
		}

		AsyncTaskResult result = new AsyncTaskResult();
		result.groundOverlayOptionsList = groundOverlayOptionsList;
		return result;
	}

	@SuppressLint("LongLogTag")
	@Override
	protected void onPostExecute(AsyncTaskResult result)
	{
		if (map.get() == null)
		{
			Log.w(TAG, "Could not proceed with the AsyncTask result, GoogleMap is null.");
			return;
		}

		for (GroundOverlay groundOverlay : currentGroundOverlay)
		{
			groundOverlay.remove();
		}

		currentGroundOverlay.clear();

		for (Pair<GroundOverlayOptions, SouthTyrolActivity> overlayOptions : result.groundOverlayOptionsList)
		{
			GroundOverlay groundOverlay = map.get().addGroundOverlay(overlayOptions.first);
			groundOverlay.setTag(overlayOptions.second);
			currentGroundOverlay.add(groundOverlay);
		}
	}

	static class AsyncTaskResult
	{
		List<Pair<GroundOverlayOptions, SouthTyrolActivity>> groundOverlayOptionsList;
	}
}
