namespace connectivity_map
{
    public class CsvDataItem
    {
        public string timestamp { get; set; }
        public string name { get; set; }
        public string radius { get; set; }
        public int duration { get; set; }
        public string state { get; set; }
        public string geohash { get; set; }
    }

    public class DataItem{
        public string Name {get;set;}
        public double lat { get; set; }
        public double lng { get; set; }
        public override bool Equals(object obj){
            return Name.Equals(((DataItem)obj).Name);
        }
        public override int GetHashCode(){
            return Name.GetHashCode();
        }
    }
    public class DataSet{
        public string timestamp { get; set; }
        public string radius { get; set; }
        public int duration { get; set; }
        public string state { get; set; }
    }
}