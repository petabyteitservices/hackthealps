package com.bosch.myspin.virtualapps.music.ui.cache;

import java.lang.ref.WeakReference;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.widget.ImageView;

import com.bosch.myspin.R;

/**
 * Subclass of {@link AsyncBitmapDrawable} caching the place holder album art.
 *
 * @author Malte Pein
 * @since 14.11.16
 */
public class BigAsyncAlbumCoverDrawable extends AsyncBitmapDrawable
{
    private static Bitmap sPlaceholderBitmap;

    /**
     * Creates a new {@link AsyncBitmapDrawable} with a {@link WeakReference} on the given {@link BitmapWorkerTask}.
     *
     * @param res
     *         the {@link Resources}.
     * @param bitmap
     *         a {@link Bitmap} as placeholder until the bitmapWorkerTask is ready and loaded the read image.
     * @param bitmapWorkerTask
     *         the {@link BitmapWorkerTask}.
     */
    private BigAsyncAlbumCoverDrawable(final Resources res, final Bitmap bitmap,
                                       final AsyncBitmapDrawable.BitmapWorkerTask bitmapWorkerTask)
    {
        super(res, bitmap, bitmapWorkerTask);
    }

    /**
     * Loads a {@link Bitmap} into a new {@link AsyncBitmapDrawable}.
     *
     * @param resources
     *         the {@link Resources}.
     * @param filename
     *         a {@link Uri} to the Bitmap to load.
     * @param imageView
     *         the {@link ImageView} to set the {@link Bitmap} to.
     */
    public static void loadBigAlbumArt(final Resources resources, final Uri filename, final ImageView imageView)
    {
        if (sPlaceholderBitmap == null)
        {
            sPlaceholderBitmap = BitmapFactory.decodeResource(resources, R.drawable
                    .music_placeholder_album_big);
        }

        AsyncBitmapDrawable.loadBitmapWithoutCache(resources, sPlaceholderBitmap, filename, imageView);
    }
}
