package de.hellwig.hackthealps_app.hotspotdata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import android.content.Context;

import de.hellwig.hackthealps_app.hotspotdata.parsing.AlyvixDeviceState;
import de.hellwig.hackthealps_app.hotspotdata.parsing.WuerthParser;

public class HotSpotProvider
{
    private final static List<HotSpot> hotSpots = new LinkedList<>();

    public HotSpotProvider(Context context)
    {
        synchronized (hotSpots)
        {
            if (hotSpots.isEmpty())
            {
                ArrayList<AlyvixDeviceState> alyvixDeviceStates = new WuerthParser().parse(context);

                Map<String, HotSpot> hotspotNameMap = new HashMap<>();

                for (AlyvixDeviceState aDS : alyvixDeviceStates)
                {
                    HotSpot hotSpot;
                    if (!hotspotNameMap.containsKey(aDS.getName()))
                    {
                        hotSpot = new HotSpot(aDS.getName(), aDS.getLocation());
                        hotspotNameMap.put(aDS.getName(), hotSpot);
                        hotSpots.add(hotSpot);
                    }
                    else
                    {
                        hotSpot = hotspotNameMap.get(aDS.getName());
                    }

                    hotSpot.addMeasurement(aDS.getDate().getTime(), (int)(aDS.getRadius() * 1000), aDS.getDuration(),
                            aDS.getState());
                }
            }
        }
    }

    public List<HotSpot> getHotSpots()
    {
        return hotSpots;
    }
}
