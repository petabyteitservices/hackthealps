package izeland.de.myapplication;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.MapStyleOptions;

import java.util.List;

import de.hellwig.hackthealps_app.mapview.DrawActivitiesOnMapAsyncTask;
import de.hellwig.hackthealps_app.mapview.ParseAndDrawHotSpotsOnMapAsyncTask;
import izeland.de.myapplication.fragments.ActivityDetailFragment;
import izeland.de.myapplication.fragments.ChildFragment;
import izeland.de.myapplication.fragments.ShowActivitiesFragment;
import izeland.de.myapplication.fragments.profile.ProfileFragment;
import izeland.de.myapplication.model.ActivityListResponse;
import izeland.de.myapplication.model.ActivityListViewModel;
import izeland.de.myapplication.model.PhoneStore;
import izeland.de.myapplication.model.SouthTyrolActivity;
import izeland.de.myapplication.utils.Utils;

public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, OnMapReadyCallback, GoogleMap.OnGroundOverlayClickListener, ChildFragment
{

	private static final String TAG = "MainActivity";

	private ViewPager viewPager;
	private BottomNavigationView navigation;
	private GoogleMap map;
	private ActivityListViewModel viewModel;

	private View modalContainer;

	private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
			= new BottomNavigationView.OnNavigationItemSelectedListener()
	{

		@Override
		public boolean onNavigationItemSelected(@NonNull MenuItem item)
		{
			switch (item.getItemId())
			{
				case R.id.navigation_profile:
					viewPager.setCurrentItem(0);
					return true;
				case R.id.navigation_dashboard:
					viewPager.setCurrentItem(1);
					return true;
				case R.id.navigation_connect:
					viewPager.setCurrentItem(2);
					return true;
			}
			return false;
		}
	};
	;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main2);

		findViewById(android.R.id.content).setBackground(getDrawable(R.drawable.background));

		// Load content:
		viewPager = findViewById(R.id.main_screen_pager);
		viewPager.setAdapter(new MainMenuFragmentPagerAdapter(getSupportFragmentManager()));
		viewPager.addOnPageChangeListener(this);

		navigation = findViewById(R.id.navigation);
		navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

		modalContainer = findViewById(R.id.modal_frag_container);
	}


	// On Page Change Callbacks:
	@Override
	public void onPageScrolled(int i, float v, int i1)
	{

	}

	@Override
	public void onPageSelected(int i)
	{
		navigation.setSelectedItemId(Utils.getNavigationId(i));
	}

	@Override
	public void onPageScrollStateChanged(int i)
	{

	}

	public void triggerUpdate()
	{
		if (viewModel != null)
		{
			List<Utils.ActivityType> activityTypes = Utils.constructSearch(PhoneStore.getInstance(getApplication()).getJournesFilterIds());
			int sum = 0;
			for (Utils.ActivityType activityType : activityTypes)
			{
				sum += activityType.getValue();
			}
			if (!activityTypes.isEmpty())
			{
				viewModel.update(sum);
			}
			else
			{
				viewModel.update(null);
			}
		}
	}

	/**
	 * Manipulates the map once available.
	 * This callback is triggered when the map is ready to be used.
	 * This is where we can add markers or lines, add listeners or move the camera. In this case,
	 * we just add a marker near Sydney, Australia.
	 * If Google Play services is not installed on the device, the user will be prompted to install
	 * it inside the SupportMapFragment. This method will only be triggered once the user has
	 * installed Google Play services and returned to the app.
	 */
	@Override
	public void onMapReady(final GoogleMap googleMap)
	{
		map = googleMap;

		map.setTrafficEnabled(true);
		map.setOnGroundOverlayClickListener(this);
		try
		{
			// Customise the styling of the base map using a JSON object defined in a raw resource file.
			boolean success = map.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json));

			if (!success)
			{
				Log.e(TAG, "Style parsing failed.");
			}
		}
		catch (Resources.NotFoundException e)
		{
			Log.e(TAG, "Can’t find style. Error: ", e);
		}

		new ParseAndDrawHotSpotsOnMapAsyncTask(this, googleMap).execute();

		viewModel = ViewModelProviders.of(this).get(ActivityListViewModel.class);
		viewModel.getActivityListObservable().observe(this, new Observer<ActivityListResponse>()
		{
			@Override
			public void onChanged(@Nullable final ActivityListResponse activitiesList)
			{
				if (activitiesList == null)
				{
					return;
				}

				SouthTyrolActivity[] array =
						activitiesList.items.toArray(new SouthTyrolActivity[activitiesList.items.size()]);
				new DrawActivitiesOnMapAsyncTask(googleMap).execute(array);
			}
		});
	}

	@Override
	public void onGroundOverlayClick(GroundOverlay groundOverlay)
	{
		if (groundOverlay.getTag() instanceof SouthTyrolActivity)
		{
			SouthTyrolActivity southTyrolActivity = (SouthTyrolActivity) groundOverlay.getTag();

			ActivityDetailFragment activityDetailFragment = new ActivityDetailFragment();
			activityDetailFragment.setSouthTyrolActivity(southTyrolActivity);
			activityDetailFragment.setChildFragmentDelegate(this);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.modal_frag_container, activityDetailFragment)
					.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,
							android.R.anim.slide_in_left, android.R.anim.slide_out_right)
					.commit();
			modalContainer.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onChildBackPress()
	{
		Fragment frag = getSupportFragmentManager().findFragmentById(R.id.modal_frag_container);
		if (frag != null)
		{
			getSupportFragmentManager().beginTransaction().remove(frag).commit();
		}
		modalContainer.setVisibility(View.GONE);
	}

	// Fragment View Pager Adapter
	private class MainMenuFragmentPagerAdapter extends FragmentPagerAdapter
	{

		private static final int PAGE_COUNT = 3;

		MainMenuFragmentPagerAdapter(FragmentManager fm)
		{
			super(fm);
		}


		@Override
		public Fragment getItem(int i)
		{
			switch (i)
			{
				case 0:
					return new ProfileFragment();
				case 1:
					return new ShowActivitiesFragment();
				case 2:
					SupportMapFragment supportMapFragmen = new SupportMapFragment();
					supportMapFragmen.getMapAsync(MainActivity.this);
					return supportMapFragmen;
				default:
					break;

			}
			return null;
		}


		@Override
		public int getCount()
		{
			return PAGE_COUNT;
		}
	}
}
